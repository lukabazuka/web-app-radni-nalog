const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
var admin = require("firebase-admin");
var serviceAccount = require('./firebase-adminsdk-qcrt5@radninalog---zavrsni-rad.iam.gserviceaccount.com'); 
admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://radninalog---zavrsni-rad-default-rtdb.europe-west1.firebasedatabase.app"
   });
   app.listen(3000, () => {
    console.log("Server running on port 3000");
   });