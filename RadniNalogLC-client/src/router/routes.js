const routes = [
  {
    path: '/',
    component: () => import('layouts/PocetnaLayout.vue')
  },
  {
    path: '/Login',
    component: () => import('layouts/LoginLayout.vue'),
    children: [
      { path: '', component: () => import('pages/LoginIndex.vue') }
    ]
  },
  {
    path: '/Izbornik',
    component: () => import('layouts/IzbornikLayout.vue'),
    meta: { auth: true }
  },
  {
    path: '/Admin',
    component: () => import('layouts/SystemLayout.vue'),
    meta: { auth: true },
    children: [
      { path: '/RadniNalog', meta: { auth: true }, component: () => import('pages/RadniNalog/RadniNalogIndex.vue') },
      { path: '/Klijenti', meta: { auth: true }, component: () => import('pages/RadniNalog/KlijentiIndex.vue') },
      { path: '/Industrija', meta: { auth: true }, component: () => import('pages/RadniNalog/IndustrijaIndex.vue') },
      { path: '/Operateri', meta: { auth: true }, component: () => import('pages/RadniNalog/OperaterIndex.vue') },
      { path: '/Lokacije', meta: { auth: true }, component: () => import('pages/RadniNalog/LokacijeIndex.vue') }
    ]
  }
]

export default routes

if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue')
  })
}
