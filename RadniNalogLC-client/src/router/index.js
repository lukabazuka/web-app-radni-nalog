import Vue from 'vue'
import VueRouter from 'vue-router'
import VueHtmlToPaper from 'vue-html-to-paper'
import * as VueGoogleMaps from 'vue2-google-maps'
import routes from './routes'

Vue.use(VueRouter)

Vue.use(VueGoogleMaps, {
  load: {
    key: 'qGBK7_nCDFtk4eaktI3sBgvJfycMT69DZcQ0d5rWJgQ'
  }
})
/*
* If not building with SSR mode, you can
* directly export the Router instantiation
*/
const options = {
  name: '_blank',
  specs: ['fullscreen=yes', 'titlebar=yes', 'scrollbars=yes'],
  styles: [
    'https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css',
    'https://unpkg.com/kidlat-css/css/kidlat.css'
  ]
}
Vue.use(VueHtmlToPaper, options)

const isUserLoggedIn = () => {
  return new Promise((resolve, reject) => {
    const unsubscribeOnAuthStateChanged = Vue.prototype.$auth.onAuthStateChanged(theUser => {
      resolve(theUser)
      unsubscribeOnAuthStateChanged()
    }, err => {
      console.error(err)
      resolve(null)
      unsubscribeOnAuthStateChanged()
    })
  })
}
export default function (/* { store, ssrContext } */) {
  const Router = new VueRouter({
    scrollBehavior: () => ({ x: 0, y: 0 }),
    routes,
    // Leave these as they are and change in quasar.conf.js instead!
    // quasar.conf.js -> build -> vueRouterMode
    // quasar.conf.js -> build -> publicPath
    mode: process.env.VUE_ROUTER_MODE,
    base: process.env.VUE_ROUTER_BASE
  })
  Router.beforeEach(async (to, from, next) => {
    console.log(Vue.prototype.$auth.currentUser)
    if (to.matched.some(record => record.meta.auth)) {
      await isUserLoggedIn()
        .then(res => {
          if (res) {
            next()
          } else {
            next('/')
          }
        })
    } else {
      next()
    }
  })
  return Router
}

/* exports.geocoodeAddressAndSave = functions.https.onRequest(
  (request, response) => {
    try {
      const adresa = request.body.adresa
      const {data} = $axios.get()
    } catch (error) {
      functions.logger.error(error.message)
      response.status(500).send()
    }
    response.send()
  }
);
*/
