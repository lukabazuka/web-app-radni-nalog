import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'
const firebaseConfig = {
  apiKey: 'AIzaSyC2ouXFBEdjjlkT52c9ttAI_xMEZ6e4x94',
  authDomain: 'radninalog---zavrsni-rad.firebaseapp.com',
  databaseURL: 'https://radninalog---zavrsni-rad-default-rtdb.europe-west1.firebasedatabase.app',
  projectId: 'radninalog---zavrsni-rad',
  storageBucket: 'radninalog---zavrsni-rad.appspot.com',
  messagingSenderId: '11982759518',
  appId: '1:11982759518:web:fa29b8cad8bbf19e5b76cf',
  measurementId: 'G-SZQX8C6JKW'
}
firebase.initializeApp(firebaseConfig)
export default ({ Vue }) => {
  Vue.prototype.$auth = firebase.auth()
  Vue.prototype.$db = firebase.firestore()
  Vue.prototype.$storage = firebase.storage()
}
